import { NavLink } from "react-router-dom";
import Wrapper from "../assets/wrappers/Navbar";
import { Link } from "react-router-dom";
export const Navbar = () => {
  return (
    <>
      <Wrapper>
        <div className="nav-center">
          <Link to="/" className="logo">
            Mix Master
          </Link>
          <div className="nav-links">
            <NavLink to="/" className="nav-link">
              Home
            </NavLink>
            <NavLink to="/about" className="nav-link">
              About
            </NavLink>
            <NavLink to="/newsletter" className="nav-link">
              Newsletter
            </NavLink>
          </div>
        </div>
      </Wrapper>
    </>
  );
};
